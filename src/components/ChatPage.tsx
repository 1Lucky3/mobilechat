import React, {useEffect, useState} from 'react';
import {
  Text,
  View,
  StyleSheet,
  TextInput,
  Image,
  ScrollView, TouchableOpacity,
} from "react-native";
import {useDispatch, useSelector} from "react-redux";
import {fetchMessages, sendMessage} from "../state/action-creators";
import useCashData from "../hooks/useCashData";
import {RootState} from "../state/reducers";


const ChatPage: React.FC = () => {
  const dispatch = useDispatch();
  const [message, setMessage] = useState<string>("");
  const { dialogId } = useCashData();
  const { data, messageKey } = useSelector((state: RootState) => state.chat)
  const handleSend = (text: string): void => {
    dispatch(sendMessage(text, dialogId))
  }
  console.log(messageKey, "from COmponent")
  useEffect( () => {
    if(dialogId) dispatch(fetchMessages(dialogId))
  },[dialogId])

  return (
    <View style={styles.container}>
      <View style={styles.navBar}>
        <Text style={styles.navBar__text}>Вы общаетесь с оператором Х</Text>
      </View>
      <Text style={styles.dialogStart}>Диалог начат в 00:00</Text>
      <ScrollView style={styles.message__container}>
        {data.map( (item,index) => (
          item.writtenBy === "operator" ? <View key={messageKey[index]} style={[styles.message__box, styles.message__box_operator]}>
            <Text style={[styles.message__text, styles.message__text_operator]}>{item.content} 1</Text>
          </View> : <View key={messageKey[index]} style={[styles.message__box, styles.message__box_user]}>
            <Text style={[styles.message__text, styles.message__text_user]}>{item.content} 1</Text>
          </View>
        ))}
      </ScrollView>
      <View style={styles.input__wrapper}>
        <TextInput
          style={styles.inputStyle}
          placeholder="Введите сообщение..."
          placeholderTextColor="white"
          onChangeText={setMessage}
          value={message}
        />
        <Image
          style={styles.input__send}
          source={require("../images/send.png")}
        />
        <TouchableOpacity
          style={styles.send__button}
          onPress={() => handleSend(message)}
        >
        </TouchableOpacity>
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
  },
  navBar: {
    height: 80,
    width: "100%",
    backgroundColor: "#3596ff",
    justifyContent: "center",
    alignItems: "flex-start",
  },
  dialogStart: {
    flex: 1,
    fontSize: 16,
  },
  message__container: {
    maxHeight: "100%",
    overflow: "scroll",
    width: "100%"
  },
  message__box: {
    margin: 10,
    borderRadius: 15,
    maxWidth: "80%",
  },
  message__box_user:{
    borderBottomEndRadius: 0,
    backgroundColor: "#007aff",
    alignSelf: "flex-end"
  },
  message__box_operator: {
    alignSelf: "flex-start",
    backgroundColor: "#d0cccc",
    borderBottomLeftRadius: 0,
  },
  message__text: {
    fontSize: 18,
    padding: 15,
  },
  message__text_user: {
    color: "white",
  },
  message__text_operator: {
    color: "black"
  },
  navBar__text: {
    color: "white",
    fontSize: 14,
    marginLeft: 10,
  },
  input__wrapper: {
    width: "100%",
    position: "relative"
  },
  inputStyle: {
    width: "100%",
    borderWidth: 2,
    borderColor: "gray",
    backgroundColor: "#3596ff",
    alignSelf: "center",
    paddingLeft: 10,
    paddingRight: 45,
  },
  input__send: {
    position: "absolute",
    right: 15,
    top: 15
  },
  send__button: {
    height: 25,
    width: 25,
    position: "absolute",
    right: 15,
    top: 15,
  }
})
export default ChatPage;