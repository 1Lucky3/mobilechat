import { useState, useEffect} from "react";
import AsyncStorage from "@react-native-async-storage/async-storage";

interface ICashData {
  isQueue: boolean;
  dialogId: string;
  chatStarted: boolean;
}

function useCashData(): ICashData {
  const [data, setData] = useState <ICashData> ({
    isQueue: false,
    dialogId: "",
    chatStarted: false
  });

  useEffect(() => {
    const queue = async () => {
      let values
      try {
        values = await AsyncStorage.multiGet(['isQueue', 'dialogId', 'chatStarted'])
      } catch(e) {
        // read error
      }
      return values;
    }
    queue().then( value => {
      const data = {}
      value.map( item => {
        if(item[0] === "isQueue" || item[0] === "chatStarted"){
          return data[item[0]] = JSON.parse(item[item.length - 1])
        }
        return data[item[0]] = item[item.length - 1]
      })
      setData(data as ICashData);
    })
  },[AsyncStorage]);
  return data
}

export default useCashData;